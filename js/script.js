	$(document).ready(function(){
		//$('#nav').localScroll(800);
		
		RepositionNav();
		
		$(window).resize(function(){
			RepositionNav();
		});	
		
		//.parallax(xPosition, adjuster, inertia, outerHeight) options:
		//xPosition - Horizontal position of the element
		//adjuster - y position to start from
		//inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
		//outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
		$('#block-1').parallax("50%", 0, 0.1, true);
		$('#block-2').parallax("50%", 0, 0.1, true);
		$('.layer-1').parallax("50%", 2700, 0.4, true);
		$('#block-3').parallax("50%", 2750, 0.3, true);
		$('.layer-2').parallax("40%", 3400, 0.5, true);
		$('#block-4').parallax("50%", 3450, 0.3, true);
		
		var deck = new $.scrolldeck({
			slides: '.slide',
			buttons: '#nav li a',
			easing: 'easeInOutExpo'
		});

	})